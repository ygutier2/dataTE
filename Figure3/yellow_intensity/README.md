# Fluorescent microscopy experiments: time evolution of the average yellow intensity 
Here we provide results from fluorescent microscopy experiments for samples at a fixed salt concentration [NaCl]=150 mM. Briefly, we mixed equimolar amounts of DNA nanostars (A and B). These nanostars are fluorescently labelled with either FAMK (green) or Cy3 (red). 

We proceed to split one image from experiments into two distinct channels and extract a $512\times512$ intensity matrix for the green ($I_g$) and one matrix for the red ($I_r$) channel. The elements of each matrix are integer numbers referring to the pixel values, which span from 0 to 255 (maximum value for an 8-bit image).

We then perform the following analysis: 
> 1.- We subtract the noise, obtained by taking an image with the same microscope settings on an empty slide, and set any negative value to zero.
>  
> 2.- Normalise the elements of both matrices by 255.
> 
> 3.- To quantify the degree of mixing, we perform an element-wise multiplication. This is, we multiply each element of $I_g$ with the corresponding element of $I_r$, to obtain a 2D map of yellow: $I_{y}(ij)=I_{g}(ij)·I_{r}(ij)$.
> 
> 4.- We compute the average and stdev of the yellow intensity across the image

By repeating the previous analysis for confocal images obtained at different times, we can extract the yellow intensity as function of time. This information can be found in the file **Iy\_vs\_time.txt** for two different concetrations of nanostars 250 uM and 500 uM (look inside the corresponding folder). We use the gnuplot script **gp\_Iy\_vs\_time** to read this information and generate **Iy\_vs\_t.pdf** (Figure 3(f)).

The average over time of the intensity of the yellow signal as a function of concentration can be found in the file **ave\_Iy.txt**. We use the gnuplot script **gp\_Iy\_vs\_C** to read this information and generate **Iy\_vs\_C.pdf** (Figure 3(f), inset).

### Technical note:
If you followed the gnuplot scripts carefully, you probably noticed that the yellow intensity was multiplied by a constant factor 0.79. Indeed, this factor is needed to compare results at the two salt concentrations reported in our paper. And although the factor it is no so important for the comparison at the two different concentrations of nanostars shared here, we tought that it was a good oportunity to explain the origin of this factor. 

In particular, we adjusted the intensity of the data at [NaCl] = 150 mM. The reason is that we used a $10\times$ magnification objective at [NaCl] = 150 mM and a $20\times$ magnification objective at [NaCl] = 500 mM. The adjustment is based on the well-known relation:

$$I\propto \frac{(NA)^{4}}{M^{2}}$$ 

Here, $I$ is the intensity, $NA$ is the numerical aperture and $M$ is the magnification. In epi-illumination fluorescence microscopy, brightness is proportional to the fourth power of $NA$ due to the objective's dual role in focusing and collecting light, and inversely proportional to  the square of $M$ because higher magnification spreads the light, reducing brightness.

Since the $20\times$ objective has a higher $NA$ (0.4) compared to the $10\times$ objective (0.3), it gathers more light but also increases magnification, which reduces brightness. To account for these differences, we rescaled the intensity by 0.79, obtained from teh following relation:

$$\begin{align}
I_{20\times} &= I_{10\times} \frac{ (NA)^4⁄M^2)_{20\times}}{ (NA)^4⁄M^2)_{10\times}}\\
             &= I_{10\times} \frac{0.4^{4}/20^{2}}{0.3^{4}/10^{2}}\\
             &=0.79 \; I_{10\times}
\end{align}
$$