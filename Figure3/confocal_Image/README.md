# Confocal Images from fluorescent microscopy experiments

Here we provide results from fluorescent microscopy experiments at high [DNAns]. Briefly, we mixed equimolar amounts of DNA nanostars (A and B), each one at a concentration of 500 uM (beyond the phase-separation region). These nanostars are fluorescently labelled with either FAMK (green) or Cy3 (red).

We proceed to split one image from experiments into two distinct channels and extract a $512\times512$ intensity matrix for the green ($I_g$) and one matrix for the red ($I_r$) channel. This information can be found in the files **green.txt** and **red.txt**. The elements of each matrix are integer numbers referring to the pixel values, which span from 0 to 255 (maximum value for an 8-bit image).

The python script **matrix\_product\_p1\_v2.py** contains the instructions to perform the following analysis:

> 1.- We subtract the noise, obtained by taking an image with the same microscope settings on an empty slide, and set any negative value to zero.
>  
> 2.- Normalise the elements of both matrices by 255.
> 
> 3.- To quantify the degree of mixing, we perform an element-wise multiplication. This is, we multiply each element of $I_g$ with the corresponding element of $I_r$, to obtain a 2D map of yellow: $I_{y}(ij)=I_{g}(ij)·I_{r}(ij)$.

The analysis can be run by typing in a terminal *python3 matrix\_product\_p1\_v2.py*. This will create the images **red.pdf**, **green.pdf**, **prod.pdf** that are shown in Figure 3(c). It will also create two files with the average and standard deviation of the yellow intensity across the whole image.