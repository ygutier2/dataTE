#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jun 11 11:00:31 2023

@author: s2200118
"""

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors 

#green matrix
matrix_green=np.loadtxt('green.txt')
print(matrix_green)

matrix_green_noback=matrix_green-0.0046
print(matrix_green_noback.min())
np.clip(matrix_green_noback, 0, None)

matrix_green_scaled=(matrix_green_noback-matrix_green_noback.min())/(255-matrix_green_noback.min())
print(matrix_green_scaled.min())

#red matrix
matrix_red=np.loadtxt('red.txt')
print(matrix_red)

matrix_red_noback=matrix_red-0.0003
print(matrix_red_noback.min())
np.clip(matrix_red_noback, 0, None)

matrix_red_scaled=(matrix_red_noback-matrix_red_noback.min())/(255-matrix_red_noback.min())
print(matrix_red_scaled.min())
# print(matrix_red.min())
# print(matrix_red)


matrix_product=matrix_green_scaled*matrix_red_scaled
mean_prod= np.mean(matrix_product)
std_prod= np.std(matrix_product)
# # matrix_product=matrix_product>0.5
# #matrix_product=np.where(matrix_product>0.5, matrix_product, 0)
print(matrix_product)
print(mean_prod)
print(std_prod)
with open('ave.txt', 'w') as file:
#     # Write the number to the file
     file.write(str(mean_prod))
with open('std_ave.txt', 'w') as file:
#     # Write the number to the file
     file.write(str(std_prod))
     
#a,b,c,d=0,100,0,100
a,b,c,d=0,-1,0,-1

# min_val, max_val sono la percentuale dal basso e dall'alto della colorbar
min_val, max_val = 0, 0.8
n =10
orig_cmap = plt.cm.Greens_r
colors = orig_cmap(np.linspace(min_val, max_val, n))
cmap = matplotlib.colors.LinearSegmentedColormap.from_list("mycmap", colors)

min_val, max_val = 0, 0.8
n =10
orig_cmap = plt.cm.Reds_r
colors = orig_cmap(np.linspace(min_val, max_val, n))
cmap_r = matplotlib.colors.LinearSegmentedColormap.from_list("mycmap", colors)

gradient = np.linspace(0, 1, 256)
gradient = np.vstack((gradient, gradient))

plt.figure()
img=plt.imshow(matrix_green_scaled[a:b,c:d], cmap=cmap)
plt.colorbar(orientation='vertical', cmap=cmap)
img.set_clim(0, 1)
img.figure.axes[1].tick_params(axis="both", labelsize=26)
plt.xticks([],fontsize=26)
plt.yticks([],fontsize=26)
plt.title('$I_{g}(i,j)$', fontsize=26)
plt.savefig('green.pdf', format="pdf",transparent=True)

plt.figure()
imr=plt.imshow(matrix_red_scaled[a:b,c:d], cmap=cmap_r)
plt.colorbar(orientation='vertical', cmap=cmap_r)
imr.set_clim(0, 1)
imr.figure.axes[1].tick_params(axis="both", labelsize=26)
plt.xticks([],fontsize=26)
plt.yticks([],fontsize=26)
plt.title('$I_{r}(i,j)$', fontsize=26)
plt.savefig('red.pdf', format="pdf",transparent=True)

plt.figure()
imp=plt.imshow(matrix_product[a:b,c:d])
plt.colorbar()
imp.set_clim(0, 1)
imp.figure.axes[1].tick_params(axis="both", labelsize=22)
plt.xticks([],fontsize=22)
plt.yticks([],fontsize=22)
plt.title('$I_{y}$(i,j)=$I_{g}(i,j)$$\cdot$$I_{r}(i,j)$', fontsize=22)
plt.savefig('prod.pdf', format="pdf", transparent=True)
