# G'p vs w

Results from microrheology simulations of the elastic ($G'$) and viscuous ($G''$) modulus as function of frequency ($\omega$), for different concentrations of DNAns, can be found inside the folder **data\_Gp\_Gpp**. The name of the files there denotes if they contain frequency vs elastic-modulus (name starts with Gwp) or frequency vs viscous-modulus (name starts with Gwpp).

The gnuplot script **gp\_MRsim\_freqSweep** is used to read the information from MR and generate the plot **FreqSweep\_simulations.pdf** (Figure 1(g)). It will also create the file **GpMR_rho.txt** that contains two columns: volume fraction, and the value of the elastic plateau.
