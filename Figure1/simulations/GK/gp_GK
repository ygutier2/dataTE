unset multiplot
reset
set pointsize 0.4
set border 3 back lt -1 lw 2
set tics nomirror scale 0.5 #out
set grid back lt 0 lw 1
set encoding iso_8859_1


##############################
unset grid
set size ratio 1
set xtics offset 0,0.4
set ylab offset 0.3,0
set ytics offset 0.4,0
set xlab offset 0,1
###############################


##########################################
#DIVERGING COLOURS
###########################################
llww=2.5
set style line 10 lw llww
set style line 9 lc rgb '#D73027' lw llww
set style line 8 lc rgb '#F46D43' lw llww
set style line 7 lc rgb '#FDAE61' lw llww
set style line 6 lc rgb '#FEE090' lw llww
set style line 5 lc rgb '#E0F3F8' lw llww
set style line 4 lc rgb '#ABD9E9' lw llww
set style line 3 lc rgb '#74ADD1' lw llww
set style line 2 lc rgb '#4575B4' lw llww
set style line 1 lc rgb '#D3D3D3' lw llww


set style line 19 lc rgb '#60D73027' lw llww
set style line 18 lc rgb '#60F46D43' lw llww
set style line 17 lc rgb '#60FDAE61' lw llww
set style line 16 lc rgb '#60FEE090' lw llww
set style line 15 lc rgb '#60E0F3F8' lw llww
set style line 14 lc rgb '#60ABD9E9' lw llww
set style line 13 lc rgb '#6074ADD1' lw llww
set style line 12 lc rgb '#604575B4' lw llww
set style line 11 lc rgb '#90D3D3D3' lw llww

############################################

#To adjust the length of the samples:
set key samplen 1
#To adjust the vertical spacing of the samples:
set key spacing 1
#To adjust the fontsize:
set key top right font "Arial,18"
#Justificacion del texto
set key Left
set key maxrow 5
#Height and width of the key box can be controlled by "height" and "width"
#set key width -2

set style fill transparent solid 0.2 noborder

##########
#FILES
##########
gkfile01="data_Goft/ave_gtTOT_vs_t_rho0.01.dat"
gkfile02="data_Goft/ave_gtTOT_vs_t_rho0.02.dat"
gkfile03="data_Goft/ave_gtTOT_vs_t_rho0.03.dat"
gkfile04="data_Goft/ave_gtTOT_vs_t_rho0.04.dat"
gkfile05="data_Goft/ave_gtTOT_vs_t_rho0.05.dat"
gkfile06="data_Goft/ave_gtTOT_vs_t_rho0.06.dat"
gkfile08="data_Goft/ave_gtTOT_vs_t_rho0.08.dat"
gkfile10="data_Goft/ave_gtTOT_vs_t_rho0.1.dat"
gkfile12="data_Goft/ave_gtTOT_vs_t_rho0.12.dat"
gkfile14="data_Goft/ave_gtTOT_vs_t_rho0.14.dat"

#G(t)=V/3k_{B}T [Pxy(t)Pxy(t+tau) + Pxz(t)Pxz(t+tau) + Pyz(t)Pyz(t+tau)].
#So the factor we need is: V/3k_{B}T. Where v=(40)**3 is the volume of the box.
# k_{B}T=1
#Conversion factor
ff=21333.33333


#################################
#Plot G(t) vs time
#################################
set log
set key bottom left font "Arial,14"
set xlabel "Time [{/Symbol t}_{Br}]" 
set ylabel "G(t) [k_{B}T/{/Symbol s}^{3}]"

set format y "10^{%L}"
set format x "10^{%L}"

#If we multiply the first column by 1000 we obtain tilmestep units
#Then, if we divide by 100 the time steps we obtain the brownian time
dt=10

set pointsize 0.3
set term qt 5

##########################
#Stretch exponential fit #
##########################

f01(x)=gp01*exp(-(x/tau01)**b01)
tau01=250000

f02(x)=gp02*exp(-(x/tau02)**b02)
tau02=250000

f03(x)=gp03*exp(-(x/tau03)**b03)
tau03=250000

f04(x)=gp04*exp(-(x/tau04)**b04)
tau04=250000

f05(x)=gp05*exp(-(x/tau05)**b05)
tau05=250000

f06(x)=gp06*exp(-(x/tau06)**b06)
tau06=250000

f08(x)=gp08*exp(-(x/tau08)**b08)
tau08=250000

f10(x)=gp10*exp(-(x/tau10)**b10)
tau10=250000

f12(x)=gp12*exp(-(x/tau12)**b12)
tau12=250000

f14(x)=gp14*exp(-(x/tau14)**b14)
tau14=250000

tii=1e4

tf01=4.5e5
tf02=5.0e5
tf03=8.6e5
tf04=6.3e5
tf05=1.0e6
tf06=1.1e6
tf08=1.5e6
tf10=1.6e6
tf12=2e6
tf14=2.6e6

fit [1e4:tf01] f01(x) gkfile01 u ($1*dt):($5+$6+$7)*ff via gp01, tau01, b01
fit [6e3:tf02] f02(x) gkfile02 u ($1*dt):($5+$6+$7)*ff via gp02, tau02, b02
fit [4e3:tf03] f03(x) gkfile03 u ($1*dt):($5+$6+$7)*ff via gp03, tau03, b03
fit [2e3:tf04] f04(x) gkfile04 u ($1*dt):($5+$6+$7)*ff via gp04, tau04, b04
fit [2e3:tf05] f05(x) gkfile05 u ($1*dt):($5+$6+$7)*ff via gp05, tau05, b05
fit [1e3:tf06] f06(x) gkfile06 u ($1*dt):($5+$6+$7)*ff via gp06, tau06, b06
fit [1e3:tf08] f08(x) gkfile08 u ($1*dt):($5+$6+$7)*ff via gp08, tau08, b08
fit [1e3:tf10] f10(x) gkfile10 u ($1*dt):($5+$6+$7)*ff via gp10, tau10, b10
fit [1e3:tf12] f12(x) gkfile12 u ($1*dt):($5+$6+$7)*ff via gp12, tau12, b12
fit [1e3:tf14] f14(x) gkfile14 u ($1*dt):($5+$6+$7)*ff via gp14, tau14, b14

####PLOT##
set xr[50:1.5e6]
set yr[0.3e-5:10]

evv=1
ppss= 0.5
ti1=1e4
tff=1.0e6

plot gkfile01 every evv u ($1*dt):($5+$6+$7)*ff w p t "{/Symbol r}=0.01" ls 1 ps ppss,\
     gkfile02 every evv u ($1*dt):($5+$6+$7)*ff w p t "{/Symbol r}=0.02" ls 2 ps ppss,\
     gkfile06 every evv u ($1*dt):($5+$6+$7)*ff w p t "{/Symbol r}=0.06" ls 6 ps ppss,\
     gkfile10 every evv u ($1*dt):($5+$6+$7)*ff w p t "{/Symbol r}=0.10" ls 8 ps ppss,\
     gkfile14 every evv u ($1*dt):($5+$6+$7)*ff w p t "{/Symbol r}=0.14" ls 10 ps ppss,\
     [1e4:tf01] f01(x) not lc rgb "black",\
     [6e3:tf02] f02(x) not lc rgb "black",\
     [1e3:tf06] f06(x) not lc rgb "black",\
     [1e3:tf10] f10(x) not lc rgb "black",\
     [1e3:tf14] f14(x) not lc rgb "black"

set term pdfcairo enh solid font "Arial,18" rounded lw 1.5
set output "plot_GK_vs_time_rho.pdf"
rep; set term qt 5; rep;
unset log x
set xr[*:*]
set yr[*:*]
