# Green-Kubo simulations

Results from GK simulations in equilibrium for different concentrations of DNAns can be found inside the folder **data\_Goft**. The files there contain the autocorrelation of the different components of the stress-tensor as function of time (averaged over five different replicas). The first column represents time, followed by the six components of the autocorrelation: PxPx, PyPy, PzPz, PxPy, PxPz, PyPz. 

The gnuplot script **gp\_GK** contains the instructions to read the information from GK and generate the plot **plot\_GK\_vs\_time\_rho.pdf** (Figure 1(h)).

The file **GpAVE\_ERRORS.txt** contains three columns:
> 1.- DNAns volume fraction.
> 
> 2-3.- At each volume fraction we ran five independent replicas of the GK simulation. By fitting a streteched exponential function to $G(t)=G'_{p}*\text{exp}((t/\tau)^{b})$, we found a value of the elastic plateau ($G'_{p}$) for each replica. The average and standard deviation of the elastic plateau (using results from different replicas) are then computed and reported in columns two and three.