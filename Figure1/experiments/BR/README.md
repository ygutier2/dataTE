# G'p vs w

Results from bulk rheology experiments of the elastic ($G'$) and viscuous ($G''$) modulus as function of frequency ($\omega$), for different concentrations of DNAns and at a Fixed temperature $T=20^{\circ}C$, can be found inside the folder **Mineral\_Oil\_Solvent\_Trap**. The files there, have a header that describes the information they contain. For instance, colum 6 represents the frequency in Hz, while columns 10 and 11 represent the elastic and viscous modulus, respectively.

The gnuplot script **gp\_BR\_FreqSweep** contains the instructions to read the information from BR and generate the plots **FreqSweep.pdf** (Figure 1(d)) and, **FreqSweep\_Master.pdf** (Fig. S10(b) left panel).

# G'p vs C (scaling)

The file **alldata\_G0\_vs\_C\_BR** contains results of the elasticity measured from Bulk Rheology experiments at different concentrations of DNAns. The six columns represent:

> 1.- DNAns concentration (C) measured before the experiment, in units of [uM].
> 
> 2.- Alpha error in concentration measured before performing the experiment, in units of [uM]. This was computed as described in the SI of our manuscript.
> 
> 3.- Crossover frequency at which $G'(\omega)=G''(\omega)$, in units of [Hz].
> 
> 4.- $G_{0}$ in units of [Pa]. This is the value of the elastic modulus at the crossover frequency.
> 
> 5.- DNAns concentration (C) measured after the experiment was performed, in units of [uM].
> 
> 6.- Alpha error in concentration measured after performing the experiment, in units of [uM].

The gnuplot script **gp\_BRscaling** can be used to generate the file **Gp\_C\_scaling.pdf** (Figure S10(b), right panel) with the plot of G'p vs C. This script will also generate **FittingWErrors.txt** obtained after fitting a power law function $G'_{p}=A* c^{b}$ to the data from experiments. The file contains four columns with the best fitting parameters and errors: *A, error in A, b, error in b*.
