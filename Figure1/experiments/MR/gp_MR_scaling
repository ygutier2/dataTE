unset multiplot
reset
set pointsize 0.4
set border 3 back lt -1 lw 2
set tics nomirror scale 0.75 #out
set grid back lt 0 lw 1
set encoding iso_8859_1

##############################
unset grid
set size ratio 1

set ylab offset  -2.5,0
set ytics offset  0.5,0

set xlab offset  0,0
set xtics offset 0,-0.3

# Increase the bottom margin to make more space for the X-label
set bmargin 3.75
###############################


##########################################
#DIVERGING COLOURS
###########################################
llww=2.0
ppss=1.75
set style line 1 lc rgb '#45CFDD' lw llww ps ppss pt 14 #Red Conrad's data
set style line 2 lc rgb '#756bb1' lw llww ps ppss pt 8  #Purple our BR data
set style line 3 lc rgb '#2ca25f' lw llww ps ppss pt 6  #Green our MR data
set style line 3 lc rgb '#53BF9D' lw llww ps ppss pt 6  #Green our MR data
set style line 4 lc rgb '#4575B4' lw llww ps ppss pt 5 #For GK from simulations
set style line 5 lc rgb '#F46D43' lw llww ps ppss pt 7 #For MR data from simulations
set style line 10 lc rgb 'black' lw 2.0 # black fitting line

set style line 20 lc rgb '#7F2AFF' dt 2 lw llww  #for rho*
############################################

#To adjust the length of the samples:
set key samplen 1
#To adjust the vertical spacing of the samples:
set key spacing 1
#Justificacion del texto
set key left
set key maxrow 8

set style fill transparent solid 0.2 noborder

############
#INPUT FILES
############
fMR="alldata_Gp_vs_C_MR.dat"

#Fit to our MR data from 300 UM, considering the error-bar in G'p
set fit errorvariables
g3(x)=A3*x**b3
fit [320:*] g3(x) fMR u 1:3:4 yerrors via b3, A3


####################
#PLOT Gp vs [DNAns]#
####################
set log
set key top left font "Arial,22" at graph 0.075, 1.0
set xlabel "C [{/Symbol m}M]" font ",22"
set ylabel "G'_{p} [Pa]"      font ",22"

set format x "10^{%L}"
set format y "10^{%L}"

set xtics font ",22"
set ytics font ",22"

#set xr[100:700]
#set yr[1e1:1e2]

set xr[90:1650]
set yr[1e1:1e3]

set pointsize 1.0
set term qt 1

plot fMR u 1:3:($2/2):4 w xyerrorbars t "MR" ls 3,\
     [300:700] g3(x) w l ls 10 dashtype 1 not
     
set term pdfcairo enh solid rounded lw 1.5
set output "Gp_C_scaling.pdf"
rep; set term qt 1; rep;
unset log

#############################
## Print scaling to a file ##
#############################
set print "FittingWErrors.txt" append
print A3,A3_err,b3,b3_err
unset print
