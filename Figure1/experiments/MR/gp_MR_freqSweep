unset multiplot
reset
set pointsize 0.4
set border 3 back lt -1 lw 2
set tics nomirror scale 0.75 #out
set grid back lt 0 lw 1
set encoding iso_8859_1

##############################
unset grid
set size ratio 1

set ylab offset  -2.5,0
set ytics offset  0.5,0

set xlab offset  0,0
set xtics offset 0,-0.3

# Increase the bottom margin to make more space for the X-label
set bmargin 3.75
###############################


##########################################
#DIVERGING COLOURS
###########################################
llww=2.5
ppss=1.75

set style line 1 lt 1 lc rgb '#F7FCFD' lw llww # very light blue-green
set style line 2 lt 1 lc rgb '#E5F5F9' lw llww # 
set style line 3 lt 1 lc rgb '#CCECE6' lw llww # 
set style line 4 lt 1 lc rgb '#99D8C9' lw llww # light blue-green
set style line 5 lt 1 lc rgb '#66C2A4' lw llww # 
set style line 6 lt 1 lc rgb '#41AE76' lw llww # medium blue-green
set style line 7 lt 1 lc rgb '#238B45' lw llww #
set style line 8 lt 1 lc rgb '#005824' lw llww # dark blue-green


set style line 1 lt 1 lc rgb '#99D8C9' lw llww # light blue-green
set style line 2 lt 1 lc rgb '#66C2A4' lw llww # 
set style line 3 lt 1 lc rgb '#41AE76' lw llww # medium blue-green
set style line 4 lt 1 lc rgb '#238B45' lw llww #
set style line 5 lt 1 lc rgb '#005824' lw llww # dark blue-green


############################################

#To adjust the length of the samples:
set key samplen 1
#To adjust the vertical spacing of the samples:
set key spacing 1
#Justificacion del texto
set key left
set key maxrow 8

set style fill transparent solid 0.2 noborder

############
#INPUT FILES
############
freq01="Gwp_Gwpp_MR/GwpLog_250uM.dat"
freq02="Gwp_Gwpp_MR/GwpLog_320uM.dat"
freq03="Gwp_Gwpp_MR/GwpLog_400uM.dat"
freq04="Gwp_Gwpp_MR/GwpLog_470uM.dat"
freq05="Gwp_Gwpp_MR/GwpLog_560uM.dat"
freq06="Gwp_Gwpp_MR/GwppLog_250uM.dat"
freq07="Gwp_Gwpp_MR/GwppLog_320uM.dat"
freq08="Gwp_Gwpp_MR/GwppLog_400uM.dat"
freq09="Gwp_Gwpp_MR/GwppLog_470uM.dat"
freq10="Gwp_Gwpp_MR/GwppLog_560uM.dat"

#Factor to convert from hertz to frequencies (set to 2*pi to change from Hz TO rad/sec)
f=1.0

########################
# PLOT Gp and Gpp vs w #
########################
set log
set xlabel "{/Symbol w} [1/s]"    font ",22"
set ylabel "G^{'}, G^{''} [Pa]" font ",22"

set format x "10^{%L}"
set format y "10^{%L}"

set xtics font ",22"
set ytics font ",22"

set xr [*:1000]
set yr [*:*]
#set yr [1e-2:4e3]

set pointsize 1.0
set term qt 1

set key font "Arial,16"
set key at f*3,1.2
set key horizontal
set key maxcols 2

p freq01 u (f*$1):2 ls 1 pt 6 w l t "250 {/Symbol m}M G^{\'}",\
  freq02 u (f*$1):2 ls 2 pt 6 w l t "320 {/Symbol m}M G^{\'}",\
  freq03 u (f*$1):2 ls 3 pt 6 w l t "400 {/Symbol m}M G^{\'}",\
  freq04 u (f*$1):2 ls 4 pt 2 w l t "470 {/Symbol m}M G^{\'}",\
  freq05 u (f*$1):2 ls 5 pt 6 w l t "560 {/Symbol m}M G^{\'}",\
  freq06 u (f*$1):2 ls 1 dt 2 w l t "250 {/Symbol m}M G^{\'\'}",\
  freq07 u (f*$1):2 ls 2 dt 2 w l t "320 {/Symbol m}M G^{\'\'}",\
  freq08 u (f*$1):2 ls 3 dt 2 w l t "400 {/Symbol m}M G^{\'\'}",\
  freq09 u (f*$1):2 ls 4 dt 2 w l t "470 {/Symbol m}M G^{\'\'}",\
  freq10 u (f*$1):2 ls 5 dt 2 w l t "560 {/Symbol m}M G^{\'\'}"
     
set term pdfcairo enh solid rounded lw 1.5
set output "FreqSweep.pdf"
rep; set term qt 1; rep;
unset log 

#################################
## Concentration Superposition ##
#################################
set log
set term qt 2;

set xlabel "a{/Symbol w} [1/s]"   font ",22"
set ylabel "bG^{'}, bG^{''} [Pa]" font ",22"

#Crossover frequency (omega0)
wc01=17.06
wc02=6.69
wc03=6.02
wc04=5.43
wc05=3.19

#Elastic modulus at crossover (G0)
Gc01=6.62
Gc02=6.64
Gc03=12.36
Gc04=20.59
Gc05=26.94

#Reference sample at 560 uM
wref=wc05
Gref=Gc05

set xr [*:f*1000]
set yr [*:*]

set key font "Arial,16"
set key at f*0.5,1.3

p freq01 u (f*$1*wref/wc01):($2*Gref/Gc01) ls 1 pt 6 w l t "250 {/Symbol m}M G^{\'}",\
  freq02 u (f*$1*wref/wc02):($2*Gref/Gc02) ls 2 pt 6 w l t "320 {/Symbol m}M G^{\'}",\
  freq03 u (f*$1*wref/wc03):($2*Gref/Gc03) ls 3 pt 6 w l t "400 {/Symbol m}M G^{\'}",\
  freq04 u (f*$1*wref/wc04):($2*Gref/Gc04) ls 4 pt 2 w l t "470 {/Symbol m}M G^{\'}",\
  freq05 u (f*$1*wref/wc05):($2*Gref/Gc05) ls 5 pt 6 w l t "560 {/Symbol m}M G^{\'}",\
  freq06 u (f*$1*wref/wc01):($2*Gref/Gc01) ls 1 dt 2 w l t "250 {/Symbol m}M G^{\'\'}",\
  freq07 u (f*$1*wref/wc02):($2*Gref/Gc02) ls 2 dt 2 w l t "320 {/Symbol m}M G^{\'\'}",\
  freq08 u (f*$1*wref/wc03):($2*Gref/Gc03) ls 3 dt 2 w l t "400 {/Symbol m}M G^{\'\'}",\
  freq09 u (f*$1*wref/wc04):($2*Gref/Gc04) ls 4 dt 2 w l t "470 {/Symbol m}M G^{\'\'}",\
  freq10 u (f*$1*wref/wc05):($2*Gref/Gc05) ls 5 dt 2 w l t "560 {/Symbol m}M G^{\'\'}",\
  [f*0.4:f*1.5] 2.5*x**2 lc rgb "black" lw 2 not,\
  [f*0.15:f*0.5] 30*x lc rgb "black" lw 2 dt 2 not


set term pdfcairo enh solid rounded lw 1.5
set output "FreqSweep_Master.pdf"
rep; set term qt 2; rep;
unset log
