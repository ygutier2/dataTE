# G'p vs w

Results from microrheology experiments of the elastic ($G'$) and viscuous ($G''$) modulus as function of frequency ($\omega$), for different concentrations of DNAns and at a temperature $T\sim25^{\circ}C$, can be found inside the folder **Gwp\_Gwpp\_MR**. The name of the files there, denotes if they contain frequency vs elastic-modulus (if the name starts with Gwp) or frequency vs viscous-modulus (if the name starts with Gwpp).

The gnuplot script **gp\_MR\_FreqSweep** contains the instructions to read the information from MR and generate the plots **FreqSweep.pdf** (Figure 1(c)) and **FreqSweep\_Master.pdf** (Figure s10(a) left panel).

# G'p vs C (scaling)

The file **alldata_Gp_vs_C_MR** contains results of the elasticity measured from Micro-Rheology experiments at different concentrations of DNAns. The five columns represent:

> 1.- DNAns concentration (C) in units of [uM] 
> 
> 2.- Error in concentration in units of [uM]. This was computed as described in the Supplementary Information of our paper.
> 
> 3.- Value of G'p in units of [Pa]
> 
> 4.- Error G'p in units of [Pa], computed from fit of $G'(\omega)$ at large frequencies.
> 
>5.- alpha error (percentage)
	
The gnuplot script **gp\_MRscaling** can be used to generate the file **Gp\_C\_scaling.pdf** (Figure S10(a) right panel) with the plot of G'p vs C. This script will also generate **FittingWErrors.txt** obtained after fitting a power law function $G'_{p}=A* c^{b}$ to the data from experiments. The file contains four columns with the best fitting parameters and errors: *A, error in A, b, error in b*.