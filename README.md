# Data Topological Elasticity
Here we provide the data and gnuplot scripts to generate the plots in the main text of our paper **Topological linking determines elasticity in limited valence networks**, *Giorgia Palombo,  Simon Weir,  Davide Michieletto and Yair Augusto Gutierrez Fosado*, [**Nature Materials**, (2025)](https://doi.org/10.1038/s41563-024-02091-9). Inside each folder, you will find README files with a detailed description of the data, scripts and expected ouput. 

We use [gnuplot](http://www.gnuplot.info) to produce the plots. We provide gnuplot scripts (inside the different folders) which contain the commands to generate the plots. These scripts can be identified by their name **gp\_ScriptName** (they all start with *gp\_*). To run the scripts simply:

>1.- Open a terminal where the gnuplot script is located.
>
>2.- Type *gnuplot* to open the gnuplot interface.
>
>3.- Run the gnuplot script by typing in the terminal: *call gp\_ScriptName*.

**Note:** Scripts to run simulations and perform analysis are deposited in the complementary repository [https://git.ecdf.ed.ac.uk/ygutier2/j1Topo](https://git.ecdf.ed.ac.uk/ygutier2/j1Topo). Following the instructions there, it is possible to generate the data stored in this repository.