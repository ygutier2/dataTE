# Topological nature of elasticity

Here we provide the file **results\_topology.txt** that contains a summary of the results from simulations. The header describes the information provided in each columns:

> 1.- The volume fraction $\rho=4\pi/3  (0.5\sigma)^3 M*N/(L^3)$, with $N=7$, $L=40\sigma$ and $M$ the number of DNAns in the simulation.
>
>2-3.-  Mesh size ($\xi$) and standard deviation obtained with baggianalysis. See also *../Figure2/mesh\_size*
>
>4-5.-  With the number ($N_{minloops}$) of unrepeated minimum loops and error. See also *../Figure2/minimum\_Loops*
>
>6-7.- The value of $G'_{p}$ (and error of the mean) obtained from Green-Kubo simulations. (see also *../Figure1/simulations/GK/GpAVE\_ERRORS.txt*)
>
>8-9.- Total linking number between minimum loops and error of the mean.


We use the gnuplot script **gp\_Z\_vs\_rho** with the instructions to generate the plot **plot\_Z\_vs\_rho.pdf** (Figure 4(g)). This script will also generate **FittingWErrors\_Valence.txt** obtained after fitting a power law function $G'_{p}=A* c^{b}$ to the linking valence above overlapping concentration. The file contains four columns with the best fitting parameters and errors: *A, error in A, b, error in b*.

Finally, the gnuplot script **gp\_TE** is used to generate the plot **plot\_TE.pdf** (Figure 4(h)).