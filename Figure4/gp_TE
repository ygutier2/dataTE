unset multiplot
reset
set pointsize 0.4
set border 3 back lt -1 lw 2
set tics nomirror scale 0.75 #out
set grid back lt 0 lw 1
set encoding iso_8859_1

##############################
unset grid
set size ratio 1

set ylab offset  -2.5,0
set ytics offset  0.5,0

set xlab offset  0,-0.3
set xtics offset 0,-0.3

# Increase the bottom margin to make more space for the X-label
set bmargin 4.0
###############################

###################
#Error propagation#
###################
errormult(a,b,da,db)=a*b*sqrt( ((da*da)/(a*a)) + ((db*db)/(b*b)) )
errordiv(a,b,da,db)=(a/b)*sqrt( ((da*da)/(a*a)) + ((db*db)/(b*b)) )
errorsum(da,db)=sqrt( (da*da) + (db*db) )
errorexp(a,da,n)=n*a**(n-1)*da


##########################################
#DIVERGING COLOURS
###########################################
llww=1.5
ps1=1.3
#New colours
set style line 2 lc rgb '#003865' lw llww ps ps1 pt 5 # GK

##########################################
#DIVERGING COLOURS
###########################################
llww=1.5
ps2=1
set style line 9 lc rgb '#D73027' lw llww
set style line 6 lc rgb '#FEE090' lw llww
set style line 5 lc rgb '#E0F3F8' lw llww
set style line 4 lc rgb '#ABD9E9' lw llww
set style line 3 lc rgb '#74ADD1' lw llww
set style line 1 lc rgb '#D3D3D3' lw llww

set style line 7 lc rgb '#FDAE61' lw llww ps ps2 pt 4
set style line 8 lc rgb '#D2001A' lw llww ps ps2 pt 7

set style line 20 lc rgb '#7F2AFF' dt 2 lw llww  #for rho*

############################################

#To adjust the length of the samples:
set key samplen 1
#To adjust the vertical spacing of the samples:
set key spacing 1
#To adjust the fontsize:
#Justificacion del texto
set key left
set key maxrow 6
#Height and width of the key box can be controlled by "height" and "width"
#set key width -2

set style fill transparent solid 0.2 noborder


set key top left font "Arial,22" at graph -0.1, 1.0
set xlabel "{/Symbol r}" font ",22"
set ylabel "G'_{p} [k_{B}T/{/Symbol s}^{3}]" font ",22"

set log
set term qt 2;

set format x "10^{%L}"
set format y "10^{%L}"

set xtics font ",22"
set ytics font ",22"

set xr[0.008:0.18]
set yr[6e-5:10]

#Green kubo
f0(x)=293.7250*x**2.4

#Mesh size at low volume fraction
f1(x)=998.355*(x**2.8)

#Mesh size at high volume fraction
f2(x)=8235.7*x**3.6


plot "results_topology.txt" u 1:6:7 w yerrorbars t "GK" ls 2,\
     [0.01:0.14]  f0(x) not ls 2,\
     [0.01:0.056] f1(x) not ls 7,\
     [0.056:*]    f2(x) not ls 7,\
     "results_topology.txt" u 1:(0.0010*$8):(0.0010*$9) w yerrorbars t "Total Lk"  ls 8,\
     "results_topology.txt" u 1:(0.0010*$8) w p not ls 8,\
     [0.008:0.055] 0.26 not ls 20,\
     '< echo "0.055 0.26"' w impulse not ls 20     

set term pdfcairo enh solid rounded lw 1.5
set output "plot_TE.pdf"
rep; set term qt 2; rep;